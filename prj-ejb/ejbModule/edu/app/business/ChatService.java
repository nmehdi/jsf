package edu.app.business;

import java.util.Map;

import javax.ejb.Remote;

import edu.app.chat.Message;



@Remote
public interface ChatService {

	public void envoyer(Message message);
	public Map<Integer, Message> getMessages();
}
