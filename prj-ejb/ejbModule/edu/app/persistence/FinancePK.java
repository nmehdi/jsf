package edu.app.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FinancePK implements Serializable{

	
	

	private static final long serialVersionUID = 4135169891775537087L;

	private int idProject;
	private int idInvestor;
	
	
	public FinancePK() {
	}

	public FinancePK(int idProject, int idInvestor) {
		super();
		this.setIdProject(idProject);
		this.setIdInvestor(idInvestor);
		
	}
	@Column(name="project_fk")
	public int getIdProject() {
		return idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}
	@Column(name="investor_fk")

	public int getIdInvestor() {
		return idInvestor;
	}

	public void setIdInvestor(int idInvestor) {
		this.idInvestor = idInvestor;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + idInvestor;
		result = prime * result + idProject;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancePK other = (FinancePK) obj;
	
		if (idInvestor != other.idInvestor)
			return false;
		if (idProject != other.idProject)
			return false;
		return true;
	}
	
	
	
	
}
