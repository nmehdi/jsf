package edu.app.persistence;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@DiscriminatorValue("Creator")
@Table(name="t_creator")
public class Creator extends Profile{
	private List<Project> projects;
	private static final long serialVersionUID = 1L;
	public Creator() {
	}
	public Creator(String login, String password, String firstName,
			String lastName, String email, String profession) {
		setLogin(login) ;
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setProfession(profession);
		
	}
	@OneToMany(mappedBy="creator" , cascade=CascadeType.REMOVE )
	public List<Project> getProjects() {
		return projects;
	}
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	@Override
	public String toString() {
		return "Creator [projects=" + projects + ", id=" + id + ", login="
				+ login + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email
				+ ", profession=" + profession + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((projects == null) ? 0 : projects.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Creator other = (Creator) obj;
		if (projects == null) {
			if (other.projects != null)
				return false;
		} else if (!projects.equals(other.projects))
			return false;
		return true;
	}
}
