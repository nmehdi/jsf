package edu.app.chat;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Message
 *
 */
@Entity
@Table(name="t_message")

public class Message implements Serializable {

	
	private int id;
	private String to;
	private String from;
	private String subject;
	private static final long serialVersionUID = 1L;

	public Message() {
		super();
	}   
	@Id    
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getTo() {
		return this.to;
	}

	public void setTo(String to) {
		this.to = to;
	}   
	public String getFrom() {
		return this.from;
	}

	public void setFrom(String from) {
		this.from = from;
	}   
	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	public void setContent(String string) {
		// TODO Auto-generated method stub
		
	}
   
}
