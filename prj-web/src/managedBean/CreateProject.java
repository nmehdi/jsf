package managedBean;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



import edu.app.business.ProjectServiceLocal;

import edu.app.persistence.Project;


@ManagedBean(name="CreateProject")
@SessionScoped
public class CreateProject {
	@EJB
	private ProjectServiceLocal projectServiceLocal ;
    private Project project = new Project();

	public CreateProject() {
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String doSaveOrUpdate(){
	String navigateTo = null ;
	projectServiceLocal.update(project);
	return navigateTo;
	
	}
	
}
