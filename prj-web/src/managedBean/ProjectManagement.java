package managedBean;



import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import edu.app.business.ProjectServiceRemote;
import edu.app.persistence.Project;


@ManagedBean(name="gestion")
@SessionScoped
public class ProjectManagement {

	@EJB
	ProjectServiceRemote  remote;
	private Project project=new Project() ;
	
	private DataModel dataModel = new ListDataModel();
	public ProjectManagement() {
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}

	public DataModel getDataModel() {
		dataModel.setWrappedData(remote.findAllProjects());
		return dataModel;
	}
	public void setDataModel(DataModel dataModel) {
		this.dataModel = dataModel;
	}
	public String update(){
		project=(Project) dataModel.getRowData();
		return "";
	}
	public String deleteProject(){
		project=(Project) dataModel.getRowData();
		remote.remove(project);
		dataModel.setWrappedData(remote.findAllProjects());
		return "";
 	}
	public String updateproject(){
		remote.update(project);
		dataModel.setWrappedData(remote.findAllProjects());
		return "";
	}

}
