package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistence.Profile;



@Stateless
public class AuthenticationService implements AuthenticationServiceRemote,
		AuthenticationServiceLocal {

	@PersistenceContext
	private EntityManager em;

	public AuthenticationService() {
	}

	public void createProfile(Profile profile) {
		em.persist(profile);
	}

	public Profile authenticate(String login, String password) {
		Profile found = null;
		String jpql = "select p from Profile p where p.login=:login and p.password=:password";
		Query query = em.createQuery(jpql);
		query.setParameter("login", login);
		query.setParameter("password", password);
		try {
			found = (Profile) query.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return found;
	}

	public List<Profile> findAllProfiles() {
		return em.createQuery("select p from Profile p").getResultList();
	}

	public boolean loginExists(String login) {
		boolean exists = false;
		String jpql = "select case when (count(p) > 0)  then true else false end from Profile p where p.login=:login";
		Query query = em.createQuery(jpql);
		query.setParameter("login", login);
		exists = (Boolean) query.getSingleResult();
		return exists;
	}

}
