package managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import edu.app.business.ProjectServiceLocal;
import edu.app.persistence.Category;
import edu.app.persistence.Project;



@ManagedBean
@ViewScoped
public class ProductBean {

	@EJB
	private ProjectServiceLocal projectServiceLocal ;
	
	private Project project = new Project() ;
	private List<Project> projects ;
	private boolean formDisplayed=false;
	private List<SelectItem> selectItemsForCategories;
	private int selectedCategoryId =-1 ;
	
	
	public ProductBean() {
	}
	
	@PostConstruct
	public void init(){
		projects = projectServiceLocal.findAllProjects();
		List<Category> categories = projectServiceLocal.findAllCategories();
		selectItemsForCategories = new ArrayList<SelectItem>(categories.size());
		for (Category category:categories){
			selectItemsForCategories.add(new SelectItem(category.getId(),category.getName()));
		}
	}
	
	public String doSaveOrUpdate() {
		String navigateTo=null;
		project.setCategory(projectServiceLocal.findCategoryById(selectedCategoryId));
		projectServiceLocal.update(project);
		projects = projectServiceLocal.findAllProjects();
		formDisplayed=false;
		return navigateTo;
	}

	public String doNew() {
		String navigateTo=null;
		selectedCategoryId=-1 ;
		setFormDisplayed(true) ;
		return navigateTo;
	}

	public String doCancel() {
		String navigateTo=null;
		project = new Project() ;
		projects = projectServiceLocal.findAllProjects();
		setFormDisplayed(false) ;
		return navigateTo;
	}

	public String doDelete() {
		String navigateTo=null;
		projectServiceLocal.remove(project);
		projects = projectServiceLocal.findAllProjects();
		setFormDisplayed(false) ;
		return navigateTo;
	}



	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public boolean isFormDisplayed() {
		return formDisplayed;
	}

	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}

	public List<SelectItem> getSelectItemsForCategories() {
		return selectItemsForCategories;
	}

	public void setSelectItemsForCategories(
			List<SelectItem> selectItemsForCategories) {
		this.selectItemsForCategories = selectItemsForCategories;
	}

	public int getSelectedCategoryId() {
		return selectedCategoryId;
	}

	public void setSelectedCategoryId(int selectedCategoryId) {
		this.selectedCategoryId = selectedCategoryId;
	}
	
	
	
}
