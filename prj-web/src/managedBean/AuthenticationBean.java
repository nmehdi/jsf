package managedBean;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import edu.app.business.AuthenticationServiceLocal;
import edu.app.persistence.Profile;
import edu.app.persistence.Administrator;
import edu.app.persistence.Creator;
import edu.app.persistence.Investor;


@ManagedBean(name = "authBean")
@SessionScoped
public class AuthenticationBean implements Serializable {

	@EJB
	private AuthenticationServiceLocal authenticationServiceLocal;

	private Profile profile = new Profile();
	private boolean loggedIn = false;
	private String profileType = "";

	private static final long serialVersionUID = 6710404278650523921L;

	public AuthenticationBean() {
	}

	public String login() {
		String navigateTo = null;
		Profile found = authenticationServiceLocal.authenticate(
				profile.getLogin(), profile.getPassword());
		if (found != null) {
			profile = found;
			loggedIn = true;
			
			if (profile instanceof Administrator) {
				profileType = "Administrator";
				navigateTo = "/pages/accueiladmin";
			} else if (profile instanceof Creator) {
				profileType = "Creator";
				navigateTo = "/pages/accueilcreator";
			} else if (profile instanceof Investor) {
				profileType = "Investor";
				navigateTo = "/pages/accueilinvestor";
			
			}
			navigateTo="AccueilA?faces-redirect=true";
			System.out.println(navigateTo);
		
		} else {
			FacesMessage message = new FacesMessage("Bad credentials!");
			FacesContext.getCurrentInstance().addMessage(
					"login_form:login_submit", message);
			loggedIn = false;
			navigateTo = null;
		}
		System.out.println(navigateTo);
		return navigateTo;
	}

	public String logout() {
		String navigateTo = null;
		loggedIn = false;
		profile = new Profile();
		profileType = "";
		navigateTo = "accueil";
		return navigateTo;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

	public String redirect() {
		return "inscri";
	}

}
