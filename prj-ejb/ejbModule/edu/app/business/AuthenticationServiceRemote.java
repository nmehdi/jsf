package edu.app.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Profile;



@Remote
public interface AuthenticationServiceRemote {
	public void createProfile(Profile profile);

	List<Profile> findAllProfiles();

	Profile authenticate(String login, String password);
}
