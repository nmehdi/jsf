package edu.app.business;

import java.util.List;

import javax.ejb.Local;

import edu.app.persistence.Profile;



@Local
public interface AuthenticationServiceLocal {
	public void createProfile(Profile profile);

	List<Profile> findAllProfiles();

	Profile authenticate(String login, String password);

	boolean loginExists(String login);
}
