package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Category;
import edu.app.persistence.Project;

/**
 * Session Bean implementation class ProjectService
 */
@Stateless
public class ProjectService implements ProjectServiceRemote, ProjectServiceLocal {
	@PersistenceContext(unitName= "myPU")
	private EntityManager em ;

    
    public ProjectService() {
    }
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> findByCategory(Category category) {
		return em.createQuery("select p from Project p where p.category=:c").setParameter("c", category).getResultList();

	}

	public List<Project> findAllProjects() {
		return em.createQuery("select p from Project p").getResultList();
	}

	public void createCategory(Category category) {
		em.persist(category);
	}

	public void saveOrUpdateCategory(Category category) {
		em.merge(category);
	}

	public Category findCategoryById(int id) {
		return em.find(Category.class, id);
	}

	public void removeCategory(Category category) {
		em.remove(em.merge(category));
	}

	public List<Category> findAllCategories() {
		return em.createQuery("select c from Category c").getResultList();
	}

	public void create(Project project) {
		em.persist(project);
	}

	public Project findById(int idProject) {
		return em.find(Project.class, idProject);
	}
	public void update(Project project) {
		em.merge(project);
		
		
	}

	
	public void remove(Project project) {
		Project managedProject = em.merge(project);
		em.remove(managedProject);
		
	}

}
