package edu.app.business;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import edu.app.chat.Client;
import edu.app.chat.Message;



@Singleton(name="cs")
@Startup
public class ChatServiceImpl implements ChatService {

	@PostConstruct
	public void init(){
		System.out.println("Chat Service D�marr�");
	}
	
	
	private Map<Integer, Message> messages=
			new HashMap<Integer, Message>();

	private int counter=1;
	
	
	@Override
	public void envoyer(Message message) {
		messages.put(counter, message);
		counter++;
		
		try {
			Client client=
			(Client) LocateRegistry.getRegistry("localhost",2000)
			.lookup("client");
		
		client.receive(message);
		
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	@Override
	public Map<Integer, Message> getMessages() {
		System.out.println("appel de get message");
		return messages;
	}

}
