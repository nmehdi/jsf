package edu.app.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Category;
import edu.app.persistence.Project;

@Remote
public interface ProjectServiceRemote {


	void create(Project project);
	Project findById(int id);
	void update(Project project);
	void remove(Project project);
	List<Project> findByCategory(Category category);

	List<Project> findAllProjects();

	void createCategory(Category category);

	void saveOrUpdateCategory(Category category);

	Category findCategoryById(int id);

	void removeCategory(Category category);

	List<Category> findAllCategories();
}
