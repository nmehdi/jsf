package edu.app.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistence.Administrator;
import edu.app.persistence.Creator;
import edu.app.persistence.Investor;
import edu.app.persistence.Profile;
import edu.app.persistence.Project;



@Remote
public interface ProfileServiceRemote {
	void create(Profile profile);
	void create(Investor investor);
	void create(Creator creator);
	void create(Administrator administrator);
	void update(Investor investor);
	void update(Creator creator);
	void update(Administrator administrator);
	void update(Profile profile);
	void remove(Profile profile);
	void remove(Investor investor);
	void remove(Creator creator);
	void remove(Administrator administrator);
	void assignProjectToInvestor(List<Project> projects, Investor investor);
	List<Profile> findAll();
	Profile findByLogin(String login);
	boolean authenticate(String login,String pwd);
	Profile findById(int id);
	String findProfile(String login);
}
