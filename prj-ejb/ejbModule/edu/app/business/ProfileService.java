package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistence.Administrator;
import edu.app.persistence.Creator;
import edu.app.persistence.Investor;
import edu.app.persistence.Profile;
import edu.app.persistence.Project;



@Stateless
public class ProfileService implements ProfileServiceRemote {
	@PersistenceContext(unitName = "myPU")
	
	private EntityManager em;

	public ProfileService() {
	}

	public void create(Profile profile) {
		em.persist(profile);
	}

	public void create(Investor investor) {
		em.persist(investor);
	}

	public void create(Creator creator) {
		em.persist(creator);
	}

	public void create(Administrator administrator) {
		em.persist(administrator);
	}

	public void update(Investor investor) {
		em.merge(investor);
	}

	@Override
	public void update(Creator creator) {
		em.merge(creator);

	}

	public void update(Administrator administrator) {
		em.merge(administrator);
	}

	public void update(Profile profile) {
		em.merge(profile);
	}

	public void remove(Profile profile) {
		em.remove(em.merge(profile));
	}

	public void remove(Investor investor) {
		em.remove(em.merge(investor));
	}

	public void remove(Creator creator) {
		em.remove(em.merge(creator));
	}

	public void remove(Administrator administrator) {
		em.remove(em.merge(administrator));
	}

	@SuppressWarnings("unchecked")
	public List<Profile> findAll() {
		return em.createQuery("select p from Profile p").getResultList();
	}

	public Profile findByLogin(String login) {
		//Profile profile=new Profile();
		Profile profile = null;
		Query query = (Query) em.createQuery("select p from Profile p where p.login=:var");
		((javax.persistence.Query) query).setParameter("var", login);
		try{
		profile =(Profile) ((javax.persistence.Query) query).getSingleResult();
		}catch (Exception e){
			
		}
		return profile;
		// return em.find(Profile.class, login);
	}
	
	public String findProfile(String login) {
		
		Profile profile = (Profile) em
				.createQuery("select p.firstName from Profile p where p.login like :var")
				.setParameter("var", login).getSingleResult();
		if (profile != null){
			String name=profile.getFirstName();
			return name;
		}
		else
			return "";
		
		//return profile;
		// return em.find(Profile.class, login);
	}
	
	/*public boolean findByLogin(String login) {
		Profile profile = (Profile) em
				.createQuery("select p from Profile p where p.login like :var")
				.setParameter("var", login).getSingleResult();
		if (profile != null)
			return true;
		else
			return false;
		// return em.find(Profile.class, login);
	}*/

	public void assignProjectToInvestor(List<Project> projects,
			Investor investor) {
		// TODO Auto-generated method stub
	}

	public boolean authenticate(String login, String password) {
		Profile profile = new Profile();
		profile = (Profile) em
				.createQuery(
						"select p from Profile p where p.login like :var and p.password like :var2")
				.setParameter("var", login).setParameter("var2", password)
				.getSingleResult();
		if (profile != null)
			return true;
		else
			return false;
	}

	@Override
	public Profile findById(int id) {
		Profile profile = (Profile) em
				.createQuery("select p from Profile p where p.id like:var")
				.setParameter("var", id).getSingleResult();
		return profile;
	}
}
