package edu.app.business;

import java.util.List;

import javax.ejb.Local;

import edu.app.persistence.Category;
import edu.app.persistence.Project;



@Local
public interface ProjectServiceLocal {
	void create(Project project);

	void update(Project project);

	void remove(Project project);

	Project findById(int id);

	List<Project> findByCategory(Category category);

	List<Project> findAllProjects();

	void createCategory(Category category);

	void saveOrUpdateCategory(Category category);

	Category findCategoryById(int id);

	void removeCategory(Category category);

	List<Category> findAllCategories();
}
