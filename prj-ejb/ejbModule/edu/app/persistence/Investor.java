package edu.app.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="t_investor")
@DiscriminatorValue("Investor")
public class Investor extends Profile{
	private List<Project> projects;
	private List<Finance> finances ;
	private static final long serialVersionUID = 1L;
	public Investor() {
	}
	public Investor(String login, String password, String firstName,
			String lastName, String email, String profession) {
		setLogin(login) ;
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setProfession(profession);
	}
	@OneToMany(mappedBy="investor" , cascade=CascadeType.REMOVE )
	public List<Project> getProjects() {
		return projects;
	}
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	@Override
	public String toString() {
		return "Investor [projects=" + projects + ", id=" + id + ", login="
				+ login + ", password=" + password + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email
				+ ", profession=" + profession + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((projects == null) ? 0 : projects.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Investor other = (Investor) obj;
		if (projects == null) {
			if (other.projects != null)
				return false;
		} else if (!projects.equals(other.projects))
			return false;
		return true;
	}
	@OneToMany(mappedBy="investor" , cascade=CascadeType.REMOVE)
	public List<Finance> getFinances() {
	if(finances==null){
		finances=new ArrayList<Finance>();
	}
		return finances;
	}

	public void setFinances(List<Finance> finances) {
		this.finances = finances;
	}
	
	
}
	