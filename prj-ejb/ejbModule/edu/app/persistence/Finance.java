package edu.app.persistence;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Finance
 *
 */
@Entity
@Table(name="t_finance")

public class Finance implements Serializable {

	private FinancePK pk ;
	private double Amount;
	private Date date;
	private int idProject;
	private Project project ;
	private Investor investor ;
	private static final long serialVersionUID = 1L;

	public Finance() {
		
	}   
	
	public Finance(double amount,  Project project,
			Investor investor, Date date) {
		this.getPk().setIdInvestor(investor.getId());
		this.getPk().setIdProject(project.getIdProject());


		this.Amount = amount;
	this.date=date ; 
		this.project = project;
		this.investor = investor;
	}

	public double getAmount() {
		return this.Amount;
	}

	public void setAmount(double Amount) {
		this.Amount = Amount;
	}   
	 
	
	public int getIdproject() {
		return this.idProject;
	}

	public void setIdproject(int idProject) {
		this.idProject = idProject;
	}
	@EmbeddedId
	public FinancePK getPk() {
		if(pk==null)
			pk = new FinancePK();
		return pk;
	}
	public void setPk(FinancePK pk) {
		this.pk = pk;
	}
	@ManyToOne
	@JoinColumn(name="project_fk",insertable=false,updatable=false)
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	@ManyToOne
	@JoinColumn(name="Investor_fk",insertable=false,updatable=false)
	public Investor getInvestor() {
		return investor;
	}
	public void setInvestor(Investor investor) {
		this.investor = investor;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
   
}
