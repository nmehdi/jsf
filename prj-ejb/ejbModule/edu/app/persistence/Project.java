package edu.app.persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Project
 *
 */
@Entity
@Table(name="t_project")

public class Project implements Serializable {

	
	private int idProject;
	
	private String title;
	private byte[] avatar;

	private Category category;
	private Creator creator;
	private Investor investor;
	private Administrator administrator;
	private String description;
	private List<Finance> finances ;
	
	private static final long serialVersionUID = 1L;

	public Project() {
		super();
	}   
	




	public Project(int idProject, String title, Category category,
			Creator creator, Investor investor, Administrator administrator,
			String description) {
		super();
		this.idProject = idProject;
		this.title = title;
		this.category = category;
		this.creator = creator;
		this.investor = investor;
		this.administrator = administrator;
		this.description = description;
	}




@Id
	public int getIdProject() {
		return this.idProject;
	}

	public void setIdProject(int idProject) {
		this.idProject = idProject;
	}   
   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	@ManyToOne
	@JoinColumn(name = "category_fk", nullable = true)
	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category2) {
		this.category = category2;
	} 
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@ManyToOne
	@JoinColumn(name="creator_fk",insertable=false,updatable=false)
	public Creator getCreator() {
		return creator;
	}

	public void setCreator(Creator creator) {
		this.creator = creator;
	}
	@ManyToOne
	@JoinColumn(name="investor_fk",insertable=false,updatable=false)
	public Investor getInvestor() {
		return investor;
	}

	public void setInvestor(Investor investor) {
		this.investor = investor;
	}
	@ManyToOne
	@JoinColumn(name="administrator_fk",insertable=false,updatable=false)
	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}




@Lob
	public byte[] getAvatar() {
		return avatar;
	}





	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}




@OneToMany(mappedBy="project" , cascade=CascadeType.REMOVE)
	public List<Finance> getFinances() {
	if(finances==null){
		finances=new ArrayList<Finance>();
	}
		return finances;
	}





	public void setFinances(List<Finance> finances) {
		this.finances = finances;
	}
}
