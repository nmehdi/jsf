package edu.app.chat;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface Client extends Remote{

	public void receive(Message  message)throws RemoteException;
	
}
