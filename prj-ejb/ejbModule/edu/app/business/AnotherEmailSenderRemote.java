package edu.app.business;

import javax.ejb.Remote;

@Remote
public interface AnotherEmailSenderRemote {
	void sendMail(String to, String subject, String messageBoby);
}
