package edu.app.persistence;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@SuppressWarnings("serial")
@Entity
@Table(name="t_administrator")
public class Administrator extends Profile{
	private List<Project> projects;
	public Administrator() {
	}
	public Administrator(String login, String password, String firstName, String lastName, String email, String profession) {
		setLogin(login) ;
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setProfession(profession);
	}
	@OneToMany(mappedBy="administrator" , cascade=CascadeType.REMOVE )
	public List<Project> getProjects() {
		return projects;
	}
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}
	public String toString() {
		return "Administrator [projects=" + projects + ", id=" + id
				+ ", login=" + login + ", password=" + password
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", profession=" + profession + "]";
	}
}
